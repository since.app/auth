// Package imports
const server = require('since.fast')
const graphql = require('since.gql')
const { router } = server

// Application imports
const env = require('./.env')
const routes = require('./lib/routes')
const { resolvers, schema } = require('./lib/graphql')

// Run server
server([router(routes), graphql(schema, resolvers)], env)
