// Package imports
const { crypt: { verify } } = require('since.util')

// Application imports
const store = require('lib/store')
const {
  AuthFailed,
  AuthTokenExpired,
  EmailVerificationExpired,
  TokenNotFound,
  UserNotFound
} = require('lib/auth/errors')
const { HashedToken } = require('lib/util')
const { Token, User } = require('lib/models')
const { TYPE_AUTH, TYPE_VERIFY } = Token

/**
 * Authentication service.
 *
 * @class
 */
class Auth {
  constructor () {
    /**
     * Authenticated user.
     *
     * @type {User}
     */
    this.user = null

    /**
     * Authenticated token.
     *
     * @type {Token}
     */
    this.token = null

    /**
     * Email verification token.
     *
     * @type {Token}
     */
    this.verify = null
  }

  /**
   * Returns the JWT bearer token payload.
   *
   * @returns {Object} Token payload
   */
  payload () {
    // Bearer token payload
    const payload = {
      sub: this.user.id,
      user: this.user.username,
      name: this.user.nickname
    }

    // Adding the email verification pending property to our JWT
    if (this.verify) payload.reg = false

    // Return the user data for our JWT
    return payload
  }

  /**
   * Attempts to authenticate a user by username and password.
   *
   * @async
   * @function                    standard
   * @param    {String}           username Username to authenticate
   * @param    {String}           password Password input
   * @returns  {Promise<Boolean>}          Authentication success
   * @throws   {Error}                     Authentication error
   */
  async standard (username, password) {
    // Load the model the user is attempting to authenticate as
    if (!(this.user = await store.find(User, { username }))) {
      throw new UserNotFound()
    }

    // Ensures users verified their email address to continue using the service
    if (!await this.verified()) {
      // Notify users their tokens expired and their accounts deleted
      throw new EmailVerificationExpired()
    }

    // Use bcrypt to compare hashes
    if (!await verify(password, this.user.password)) {
      throw new AuthFailed()
    }
  }

  /**
   * Attempts to authenticate a user by token.
   *
   * @async
   * @function                    tokenized
   * @param    {String}           token     Token to authenticate
   * @returns  {Promise<Boolean>}           Authentication success
   * @throws   {Error}                      Authentication error
   */
  async tokenized (token) {
    // Look for the hashed token to make sure the token exists
    if (!(this.token = await store.find(Token, HashedToken.finder(token)))) {
      throw new TokenNotFound()
    }

    // Check to make sure the token isn't expired
    if (this.token.type === TYPE_AUTH && this.token.expires < new Date()) {
      // Delete the model if the token is expired
      this.token.delete()

      // And reject the authentication attempt
      throw new AuthTokenExpired()
    }

    // Attempt to load the user
    if (!(this.user = await store.one(User, this.token.user))) {
      // User not found so delete the related token
      await store.remove(Token, this.token.id)

      // Authentication failed: user doesn't exist
      throw new UserNotFound()
    }

    // Ensures users verified their email address to continue using the service
    if (!await this.verified()) {
      // Notify users their tokens expired and their accounts deleted
      throw new EmailVerificationExpired()
    }
  }

  /**
   * Attempts to load a verification token for the given user.
   *
   * @async
   * @function                    verified
   * @returns  {Promise<Boolean>}          Verification token not found or not expired
   */
  async verified () {
    // Load the user's email verification token
    this.verify = await store.find(Token, { user: this.user.id, type: TYPE_VERIFY })

    // Make sure the user is registering within the time frame we gave them
    if (this.verify && this.verify.expires < new Date()) {
      // Expired tokens are deleted
      this.verify.delete()

      // Delete any expired registration data
      this.user.delete()

      // Fail user verification when their account is deleted
      return false
    }

    // No email verification token found or token is still valid
    return true
  }
}

// Module exports our authentication service
module.exports = new Auth()
