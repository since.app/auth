/**
 * Generic authentication failed error.
 *
 * @extends Error
 */
class AuthFailed extends Error {
  constructor () {
    super('Invalid authentication attempt')
  }
}

/**
 * Authentication token expire error.
 *
 * @extends Error
 */
class AuthTokenExpired extends Error {
  constructor () {
    super('Authentication token expired')
  }
}

/**
 * Email verificatino token expired error.
 *
 * @extends Error
 */
class EmailVerificationExpired extends Error {
  constructor () {
    super('Verification token expired')
  }
}

/**
 * Email verification not completed error.
 *
 * @extends Error
 */
class EmailVerificationRequired extends Error {
  constructor () {
    super('Please verify your email address')
  }
}

/**
 * Authentication token not found error.
 *
 * @extends AuthFailed
 */
class TokenNotFound extends AuthFailed {}

/**
 * Duplicate user found error.
 *
 * @extends Error
 */
class UserDuplicateFound extends Error {
  constructor () {
    super('User already exists')
  }
}

/**
 * Authentication user not found error.
 *
 * @extends AuthFailed
 */
class UserNotFound extends AuthFailed {}

// Module exports available authentication errors
module.exports = {
  AuthFailed,
  AuthTokenExpired,
  EmailVerificationExpired,
  EmailVerificationRequired,
  TokenNotFound,
  UserDuplicateFound,
  UserNotFound
}
