// Application imports
const auth = require('lib/auth')
const createTokenAuth = require('./createTokenAuth')

/**
 * Creates a token in our database.
 *
 * This is the main authentication method.
 *
 * @async
 * @function                  createToken
 * @param   {Object}          _                   ...
 * @param   {Object}          args                Query variables
 * @param   {Object}          args.input          User input
 * @param   {String}          args.input.username Username input
 * @param   {String}          args.input.password Password input
 * @returns {Promise<Object>}                     Created token
 */
module.exports = async function createToken (_, { input }) {
  // Attempts to authenticate the user or throws an error
  await auth.standard(input.username, input.password)

  // Creates, persists, and returns a JWT session
  return createTokenAuth()
}
