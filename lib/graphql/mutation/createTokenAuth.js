// Package imports
const jwt = require('jsonwebtoken')

// Application imports
const auth = require('lib/auth')
const store = require('lib/store')
const { token: { duration, secret } } = require('lib/env')
const { Token } = require('lib/models')
const { Expires, HashedToken } = require('lib/util')
const { TYPE_AUTH } = Token

module.exports = async function createTokenAuth () {
  // Date object pre-configured with our token duration offset
  const expires = new Expires(duration)

  // Tokens are random 64 byte strings that have a custom "hashed" property
  const token = new HashedToken(64)

  // Creating the token model and hashing the token string
  const model = await store.add(Token, {
    expires,
    user: auth.user.id,
    type: TYPE_AUTH,
    token: token.hash
  })

  // Bearer token options
  const options = {
    expiresIn: expires.duration
  }

  // Create the signed JWT
  const bearer = jwt.sign(auth.payload(), secret, options)

  // Restoring the cleartext token and setting the bearer to be returned
  model.set({ bearer, token })

  // Return the created authentication token
  return model
}
