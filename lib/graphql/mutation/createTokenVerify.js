// Package imports
const mail = require('since.mail')

// Application imports
const auth = require('lib/auth')
const store = require('lib/store')
const { Expires, HashedToken } = require('lib/util')
const { Token } = require('lib/models')
const { TYPE_VERIFY } = Token

/**
 * Registration verification URL.
 *
 * @type {String}
 */
const REGISTER_URL = 'http://localhost:3000/#v.'

module.exports = async function createTokenVerify ({ nickname, username }) {
  // Allow users a day to register
  const expires = new Expires('24 hours')

  // Generate random 64 byte token string that has a custom "hashed" property
  const token = new HashedToken(64)

  // Create an email verification token
  const model = await store.add(Token, {
    expires,
    user: auth.user.id,
    type: TYPE_VERIFY,
    token: token.hash
  })

  // Generating the registration verification link
  const url = REGISTER_URL + token

  // Send the verification email
  mail().register(username, { nickname, url, username }).catch((e) => console.error(e))

  // Return the created verification token
  return model
}
