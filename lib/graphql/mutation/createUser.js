// Package imports
const { hash } = require('bcrypt')

// Application imports
const auth = require('lib/auth')
const createTokenAuth = require('./createTokenAuth')
const createTokenVerify = require('./createTokenVerify')
const store = require('lib/store')
const { UserDuplicateFound } = require('lib/auth/errors')
const { User } = require('lib/models')

/**
 * Creates a user in our database.
 *
 * @async
 * @function createUser
 * @param   {Object}          _                   ...
 * @param   {Object}          args                Query variables
 * @param   {Object}          args.input          User input
 * @param   {String}          args.input.username Username input
 * @param   {String}          args.input.password Password input
 * @param   {String}          args.input.nickname Nickname input
 * @returns {Promise<Object>}                     Created user
 */
module.exports = async function createUser (_, { input }) {
  // Keep a copy of the cleartext password
  const { nickname, password, username } = input

  // Make sure the username doesn't already exist
  if (await store.exists(User, { username })) throw new UserDuplicateFound()

  // Creating the database payload for our user
  const creates = {
    username,
    // Hash password using bcrypt
    password: await hash(password, 10),
    // Set default nickname
    nickname: nickname || username
  }

  // Create a user model in our database and set them as the current authenticated user
  auth.user = await store.add(User, creates)

  // Creates and sends an email verification token
  auth.verify = await createTokenVerify(creates)

  // Allow guests to use the service until they verify their email or the token expires
  return createTokenAuth()
}
