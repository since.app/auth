// Application imports
const auth = require('lib/auth')
const store = require('lib/store')
const { Token } = require('lib/models')
const { TYPE_AUTH, TYPE_VERIFY } = Token
const createTokenAuth = require('./createTokenAuth')

/**
 * Deletes a database token(JWT session, email verification, etc.).
 *
 * @async
 * @function                 deleteToken
 * @param   {Object}         _                ...
 * @param   {Object}         args             args Query variables
 * @param   {Object}         args.input       User input
 * @param   {Object}         args.input.token Token input
 * @returns {Promise<Boolean>}                Token deleted
 */
module.exports = async function deleteToken (_, { input }) {
  console.log();
  console.log('delete token');
  console.log(input.token);
  // Attempt to authenticate the token
  await auth.tokenized(input.token)
  console.log('done auth');
  console.log();

  // Deleting different token types has different effects
  const type = auth.token.type

  // Logout simply destroys auth tokens
  if (type === TYPE_AUTH) return persistLogout()

  // Register needs delete the verification token and create an auth session for the user
  if (type === TYPE_VERIFY) return persistRegister()

  // Invalid token type somehow
  throw new Error(`Cannot delete token type "${type}" (${typeof type})`)
}

// Attempts to delete an authentication token from our database
async function persistLogout () {
  return store.remove(Token, auth.token.id)
}

// Attempts to delete a verification token and issue an authentication one
async function persistRegister () {
  // Clearing auth verification token so the JWT isn't signed with { reg: false }
  if (await store.remove(Token, auth.verify.id)) auth.verify = null

  // Creating a new JWT session for the user
  return createTokenAuth()
}
