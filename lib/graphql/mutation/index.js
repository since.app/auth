// Application imports
const createToken = require('./createToken')
const createUser = require('./createUser')
const deleteToken = require('./deleteToken')
// const deleteUser = require('./deleteUser')
const updateToken = require('./updateToken')
const updateUser = require('./updateUser')

// Module exports default object containing all Fastify GraphQL mutation resolvers
module.exports = {
  createToken,
  createUser,
  deleteToken,
  // deleteUser,
  updateToken,
  updateUser
}
