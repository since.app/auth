// Package imports
const jwt = require('jsonwebtoken')

// Application imports
const auth = require('lib/auth')
const store = require('lib/store')
const { token: { duration, secret } } = require('lib/env')
const { Expires } = require('lib/util')
const { Token } = require('lib/models')

/**
 * Refreshes a JWT.
 *
 * @async
 * @function                 updateToken
 * @param   {Object}         _                ...
 * @param   {Object}         args             args Query variables
 * @param   {Object}         args.input       User input
 * @param   {Object}         args.input.token Token input
 * @returns {Promise<Token>}                  Updated token
 */
module.exports = async function updateToken (_, { input }) {
  // Attempt to authenticate the token
  await auth.tokenized(input.token)

  // Updated token expiry time
  const expires = new Expires(duration)

  // Bearer token payload
  const payload = {
    sub: auth.user.id,
    user: auth.user.username,
    name: auth.user.nickname
  }

  // Bearer token options
  const options = {
    expiresIn: expires.duration
  }

  // Authenticating via token gives us access to it which we can quickly update
  const model = await store.update(Token, { id: auth.token.id, expires })

  // Updating the returned bearer token with the new expiration
  const bearer = jwt.sign(payload, secret, options)

  // Since we hash our tokens in the database, we need to reset the return model's token
  model.set({ bearer, token: input.token })

  // Returning the updated token
  return model
}
