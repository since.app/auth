// Application imports
const version = require('./version')

// Module exports default GraphQL query resolvers
module.exports = {
  version
}
