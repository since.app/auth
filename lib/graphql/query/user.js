// Application imports
const store = require('lib/store')
const { User } = require('lib/models')

/**
 * Finds a user by UUID.
 *
 * @async
 * @function user
 * @param   {Object}          _       ...
 * @param   {Object}          args    Query variables
 * @param   {String}          args.id User ID
 * @returns {Promise<Object>}         User object
 */
module.exports = async function user (_, { id }) {
  return store.one(User, id)
}
