// Application imports
const store = require('lib/store')
const { User } = require('lib/models')

/**
 * Finds multiple users by filter.
 *
 * @async
 * @function users
 * @param   {Object}            _           ...
 * @param   {Object}            args        Query variables
 * @param   {Object}            args.filter Optional filtering
 * @returns {Promise<Object[]>}             Users matching filter
 */
module.exports = async function users (_, { filter }) {
  return store.filter(User, filter)
}
