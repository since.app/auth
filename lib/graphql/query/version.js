// Application imports
const pkg = require('../../../package.json')

// Module exports default function returning the current package.json version
module.exports = function version () {
  return pkg.version
}
