// Package imports
const { Date } = require('since.gql').resolvers

// Application imports
const Mutation = require('./mutation')
const Query = require('./query')

// Module exports default resolvers for Fastify GraphQL
module.exports = {
  Date,
  Mutation,
  Query
}
