// Package imports
const path = require('path').resolve
const { schema } = require('since.gql')

/**
 * List of directories to import GraphQL schema from.
 *
 * @type {Array}
 */
const dirs = [
  'inputs',
  'types'
]

// Module exports the async GraphQL schema builder that reads our local directories
module.exports = schema(dirs.map((dir) => path(__dirname, dir)))
