// Package imports
const mongoose = require('mongoose')
const { str: { uuidv4 } } = require('since.util')

/**
 * Enum value for authentication tokens.
 *
 * @type {String}
 */
const TYPE_AUTH = 'auth'

/**
 * Enum value for email verification tokens.
 *
 * @type {String}
 */
const TYPE_VERIFY = 'verify'

/**
 * Mongoose model name.
 *
 * @type {String}
 */
const name = 'Token'

/**
 * Mongoose model schema.
 *
 * @type {Schema}
 */
const schema = new mongoose.Schema({
  _id: {
    type: String,
    default: uuidv4
  },
  user: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true,
    enum: [TYPE_AUTH, TYPE_VERIFY]
  },
  token: {
    type: String,
    required: true
  },
  // Bearer token is returned in the GraphQL response but isn't saved in the database
  bearer: {
    type: String
  },
  // All tokens expire
  expires: {
    type: Date,
    required: true
  }
}, {
  timestamps: {
    createdAt: 'created',
    updatedAt: 'updated'
  }
})

// Module exports the Mongoose model
module.exports = mongoose.model(name, schema)

// Module also exports the name of the model for late static binding
module.exports.class = name

// Module also exports the raw Mongoose schema
module.exports.schema = schema

// Module also exports the token type enums
module.exports.TYPE_AUTH = TYPE_AUTH
module.exports.TYPE_VERIFY = TYPE_VERIFY
