// Package imports
const mongoose = require('mongoose')
const { str: { uuidv4 } } = require('since.util')

/**
 * Mongoose model name.
 *
 * @type {String}
 */
const name = 'User'

/**
 * Mongoose model schema.
 *
 * @type {Schema}
 */
const schema = new mongoose.Schema({
  _id: {
    type: String,
    default: uuidv4
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  nickname: {
    type: String
  }
}, {
  timestamps: {
    createdAt: 'created',
    updatedAt: 'updated'
  }
})

// Module exports the Mongoose model
module.exports = mongoose.model(name, schema)

// Module also exports the name of the model for late static binding
module.exports.class = name

// Module also exports the raw Mongoose schema
module.exports.schema = schema
