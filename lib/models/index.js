// Application imports
const Token = require('./Token')
const User = require('./User')

// Module exports our Mongoose models
module.exports = {
  Token,
  User
}
