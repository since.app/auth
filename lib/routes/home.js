/**
 * Home route configuration.
 *
 * @type {Array}
 */
module.exports = {
  url: '/',
  method: 'GET',
  handler: async () => {
    return '@todo redirect'
  }
}
