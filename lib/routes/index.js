// Application imports
const routes = [
  require('./home')
]

/**
 * Module exports default flattened array of route configurations.
 *
 * @type {Object[]}
 */
module.exports = routes.reduce((a, v) => a.concat(v), [])
