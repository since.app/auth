// Package imports
const { randomBytes } = require('crypto')
const { crypt: { once } } = require('since.util')

/**
 * Date constructor that offsets its internal time by the environment configured amount.
 *
 * @class
 */
class Expires extends Date {
  /**
   * Creates a date object set in the future by an expiry duration(e.g. 10 minutes).
   *
   * @param {Number|String} duration Number of seconds or a string to parse
   */
  constructor (duration) {
    super()

    if (typeof duration === 'number') {
      // We count numbers as seconds
      this.setSeconds(this.getSeconds() + duration)

      // Update the JWT duration
      this.duration = duration + 's'
    } else {
      if (typeof duration !== 'string') throw new TypeError('Expires duration must be a number or a string')

      // Extracting method names and parameters to set expiry time
      const offset = parseInt(duration, 10)
      const type = duration.replace(offset, '').trim()
      const method = type[0].toUpperCase() + type.slice(1)
      const getter = 'get' + method
      const setter = 'set' + method

      // Adjusting the expiry time on construction
      this[setter](this[getter]() + offset)

      // Expose our configured token duration
      this.duration = offset + type[0]
    }
  }
}

/**
 * Token constructor that creates random string and also provides a hashed version.
 *
 * @class
 */
class HashedToken extends String {
  /**
   * Creates a random base64 token string from a given number of bytes.
   *
   * @param {Number} bytes Number of random bytes to generate
   */
  constructor (bytes) {
    // Tokens are random strings of a given byte length
    const token = randomBytes(bytes).toString('base64')

    // Setting the string value of this token to the cleartext version
    super(token)

    // Hashing the token to simplify saving them safely to the database
    this.hash = once(token)
  }

  /**
   * Returns a query object with the hashed version of the token.
   *
   * @static
   * @param   {String} token Unhashed token to find
   * @returns {Object}       Object containing hashed token
   */
  static finder (token) {
    return { token: once(token) }
  }
}

// Module exports all of our utility classes and functions
module.exports = {
  Expires,
  HashedToken
}
